import {
    FETCH_ERROR,
    FETCH_START,
    FETCH_SUCCESS,
    SALE_STRUCTURE_DATA,
    REFRESH_TREE
} from 'constants/ActionTypes';
import axios from 'util/Api';
import { push } from 'connected-react-router'

export const getStructure = () => {
    console.log(localStorage.getItem("token"))
    return (dispatch) => {
      dispatch({type: FETCH_START});
      axios.get('v1/sale_structures',
      ).then(({data}) => {
        console.log("fetchSaleStructure: ", data);
        if (data.success) {
          dispatch({type: FETCH_SUCCESS});
          dispatch({type: SALE_STRUCTURE_DATA, payload: data.data});
          dispatch({type: REFRESH_TREE, payload: false});
        } else {
          dispatch({type: FETCH_ERROR, payload: data.message});
          dispatch({type: REFRESH_TREE, payload: false});
        }
      }).catch(function (error) {
        if (error.response) {
          if(error.response.status==401){
            dispatch(push('/signin'))
          }
        }
        dispatch({type: FETCH_ERROR, payload: error.message});
        console.log("Error****:", error.message);
      });
    }
};
export const addChild = (childobj) => {
  return (dispatch) => {
    dispatch({type: FETCH_START});
    axios.post('v1/sale_structures',childobj
    ).then(({data}) => {
      console.log("fetchSaleStructure: ", data);
      if (data.success) {
        dispatch({type: FETCH_SUCCESS});
        dispatch({type: REFRESH_TREE, payload: true});
      } else {
        dispatch({type: FETCH_ERROR, payload: data.message});
      }
    }).catch(function (error) {
        console.log(JSON.stringify(error))
      dispatch({type: FETCH_ERROR, payload: error.message});
      console.log("Error****:", error.message);
    });
  }
};
export const editChild = (id, text) => {
  return (dispatch) => {
    dispatch({type: FETCH_START});
    axios.patch('v1/sale_structures/'+id,{
      title : text
    }
    ).then(({data}) => {
      console.log("fetcheditStructure: ", data);
      if (data.success) {
        dispatch({type: FETCH_SUCCESS});
        dispatch({type: REFRESH_TREE, payload: true});
      } else {
        dispatch({type: FETCH_ERROR, payload: data.message});
      }
    }).catch(function (error) {
        console.warn(JSON.stringify(error))
      dispatch({type: FETCH_ERROR, payload: error.message});
      console.log("Error****:", error.message);
    });
  }
};
export const deleteChild = (id) => {
  return (dispatch) => {
    dispatch({type: FETCH_START});
    axios.post('v1/sale_structures/'+id
    ).then(({data}) => {
      console.log("fetchSaleStructure: ", data);
      if (data.success) {
        dispatch({type: FETCH_SUCCESS});
        dispatch({type: REFRESH_TREE, payload: true});
      } else {
        dispatch({type: FETCH_ERROR, payload: data.message});
      }
    }).catch(function (error) {
        console.log(JSON.stringify(error))
      dispatch({type: FETCH_ERROR, payload: error.message});
      console.log("Error****:", error.message);
    });
  }
};