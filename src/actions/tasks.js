import {
    FETCH_ERROR,
    FETCH_START,
    FETCH_SUCCESS,
    TASKS_DATA,
    AGENTS_DATA,
    REFRESH_TREE
} from 'constants/ActionTypes';
import axios from 'util/Api'

export const getTasks = (payload) => {
  console.log(payload)
    return (dispatch) => {
      dispatch({type: FETCH_START});
      axios.get('tasks', {
        params: {
          date: payload.date
        }},{ handlerEnabled: true }
      ).then(({data}) => {
        console.log("fetchTasks: ", data);
        if (data.status=200) {
          dispatch({type: FETCH_SUCCESS});
          dispatch({type: TASKS_DATA, payload: data.data});
        } else {
          dispatch({type: FETCH_ERROR, payload: data.message});
        }
      }).catch(function (error) {
          console.log(JSON.stringify(error))
        dispatch({type: FETCH_ERROR, payload: error.message});
        console.log("Error****:", error.message);
      });
    }
};
export const getAgents = () => {
  return (dispatch) => {
    dispatch({type: FETCH_START});
    axios.get('agents/list',{ handlerEnabled: true }
    ).then(({data}) => {
      console.log("fetchAgents: ", data);
      if (data.status=200) {
        dispatch({type: FETCH_SUCCESS});
        dispatch({type: AGENTS_DATA, payload: data.data});
      } else {
        dispatch({type: FETCH_ERROR, payload: data.message});
      }
    }).catch(function (error) {
        console.log(JSON.stringify(error))
      dispatch({type: FETCH_ERROR, payload: error.message});
      console.log("Error****:", error.message);
    });
  }
};
