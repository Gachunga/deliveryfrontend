import React from 'react';
import {connect} from 'react-redux';
import ContainerHeader from 'components/ContainerHeader/index';
import IntlMessages from 'util/IntlMessages';
import Tree from 'react-d3-tree';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import clone from 'clone';
import {getStructure, addChild, editChild} from 'actions/sale_structure';
import {getCategories, addCategory, editCategory } from 'actions/categories';

import {getReps, attachRep} from 'actions/salesreps';
import InfoView from 'components/InfoView';
import FormDialog from './FormDialog';
import ProductDialog from './ProductDialog';
import CategoryDialog from './CategoryDialog';
import SalesRepDialog from './salesRepDialog';
import SimpleDialog from './simple/SimpleDialog';

const containerStyles = {
  width: '100%',
  height: '100vh',
}

class NodeLabel extends React.PureComponent {
  state = {
    anchorEl: undefined,
    open: false,
    opendialog : false,
    edit : false,
    openproductdialog : false,
    opencategorydialog : false,
    opensalesrepdialog : false,
    opensalesreps : false
  };

  handleClick = event => {
    event.preventDefault();
    this.setState({open: true, anchorEl: event.currentTarget});
  };

  handleRequestClose = () => {
    this.setState({open: false});
  };
  /*Subfunction with conditional rule for expanding children*/
  handleNodeClick = (nodeData, event) => {
    const myRef = this.props.tree;
    const data = clone(myRef.state.data);
    
    const nodeId = this.props.nodeData.id
    const matches = myRef.findNodesById(nodeId, data, []);
    const targetNode = matches[0];

    this.expandChildren(targetNode, myRef, data)
  }
    /*Function to expand the children*/
    expandChildren = (targetNode, myRef, data) => {
      myRef.setState({ data, isTransitioning: true });
        if(targetNode._collapsed){
          if(targetNode.hasOwnProperty('_children')&&targetNode._children.length>0){
            myRef.expandNodeCustom(targetNode)
          }
         
        }else{
          myRef.collapseNodeCustom(targetNode)
        }
  
        myRef.setState({ data, isTransitioning: true });
  
        setTimeout(
            () => myRef.setState({ isTransitioning: false }),
            myRef.props.transitionDuration + 10,
        );
        myRef.internalState.targetNode = targetNode;
    }
  handleAddChild = () => {
    this.handleRequestClose();
    this.setState({opendialog: true});
  };
  handleEditChild = () => {
    this.handleRequestClose();
    this.setState({opendialog: true, edit : true});
  };
  onDialogClick = (value, text) => {
    this.setState({opendialog : false})
    if(value == "submit"){
      if(text ==""){
        return;
      }
      let data = { title : text, parent_id : this.props.nodeData.cat_id}
      this.props.addChild(data)
    }
    if(value == "edit"){
      if(text ==""){
        return;
      }
      this.props.editChild(this.props.nodeData.cat_id, text)
    }
  }
  handleAddCategory = () => {
    this.handleRequestCategoryClose();
    this.setState({opencategorydialog: true});
  };
  handleRequestCategoryClose = () => {
    this.handleRequestClose();
    this.setState({opencategorydialog: false});
  };
  onCategoryDialogClick = (value, text) => {
    this.setState({opencategorydialog : false})
    if(value == "submit"){
      if(text ==""){
        return;
      }
      let data = { name : text, structure_id : this.props.nodeData.cat_id }
      this.props.addCategory(data)
    }
    if(value == "edit"){
      if(text ==""){
        return;
      }
      this.props.editCategory(this.props.nodeData.cat_id, text)
    }
  }
  handleAddProduct = () => {
    this.handleRequestClose();
    this.setState({openproductdialog: true});
  };
  onProductDialogClose = (value) => {
    this.setState({openproductdialog : false})
  }
  handleAddSalesRep = () => {
    this.handleRequestClose();
    this.setState({opensalesrepdialog: true});
  };
  onSalesDialogClose = (value) => {
    this.setState({opensalesrepdialog : false})
  }
  handleAttachSalesRep = () => {
    this.handleRequestClose();
    this.setState({opensalesreps: true});
  };
  onSalesRepsDialogClose = (value) => {
    this.setState({opensalesreps : false})
  }
  attachRep = (id, structure_id) => {
    this.setState({opensalesreps : false})
    this.props.attachRep(id, structure_id)
  }
  handleViewProducts = () => {
    this.props.history.push('/app/products');
  };
  handleViewChannels = () => {
    this.props.history.push('/app/channels');
  };
  render() {
    console.log(this.props)
    const {className, nodeData} = this.props
    const hasplusicon = (nodeData.hasOwnProperty('_children')&&nodeData._children.length)
    return (
      <div className={className}>
        <Button
          aria-owns={this.state.open ? 'simple-SidenavContent.js' : null}
          aria-haspopup
          onClick={(nodeData, event) => { this.handleNodeClick(nodeData, event) }}
          variant="contained" 
          color="primary" 
          className="jr-btn bg-purple text-white"
        >
          {hasplusicon && <i className="zmdi zmdi-plus-square zmdi-hc-fw zmdi-hc-lg"/>}
          {nodeData.name}
        </Button>
        <Button
          aria-owns={this.state.open ? 'simple-SidenavContent.js' : null}
          aria-haspopup
          variant="contained"
          onClick = {(event) => this.handleClick(event)} 
          color="primary" 
          className="jr-btn bg-orange text-white"
        >
          <i className="zmdi zmdi-more zmdi-hc-fw zmdi-hc-lg"/>
        </Button>
        <Menu
          id="simple-menu"
          anchorEl={this.state.anchorEl}
          open={this.state.open}
          onClose={this.handleRequestClose}
        >
          <MenuItem onClick={this.handleAddChild}>Add Unit</MenuItem>
          <MenuItem onClick={this.handleEditChild}>Edit Unit</MenuItem>
          <MenuItem onClick={this.handleAddCategory}>Add Category</MenuItem>
          {nodeData.depth===1&&<MenuItem onClick={this.handleAddProduct}>Add Product</MenuItem>}
          <MenuItem onClick={this.handleAddSalesRep}>Add Sales Rep</MenuItem>
          <MenuItem onClick={this.handleAttachSalesRep}>Attach Sales Rep</MenuItem>
          <MenuItem onClick={this.handleViewProducts}>View Products</MenuItem>
          <MenuItem onClick={this.handleViewChannels}>View channels</MenuItem>
          <MenuItem onClick={this.handleEditChild}>View Active Sales Reps</MenuItem>
        </Menu>
        <FormDialog open={this.state.opendialog} edit={this.state.edit} onClick={ this.onDialogClick } />
        <CategoryDialog open={this.state.opencategorydialog} edit={this.state.editcategory} onClick={ this.onCategoryDialogClick } />
        <ProductDialog open={this.state.openproductdialog} edit={this.state.edit} onClose={ this.onProductDialogClose } structure_id = {nodeData.cat_id}/>
        <SalesRepDialog open={this.state.opensalesrepdialog} edit={this.state.edit} onClose={ this.onSalesDialogClose } structure_id = {nodeData.cat_id}/>
        <SimpleDialog users={this.props.salesreps}
                      selectedValue={this.state.selectedValue}
                      open={this.state.opensalesreps}
                      onClose={this.onSalesRepsDialogClose.bind(this)}
                      attachRep = {this.attachRep.bind(this)}
                      structure_id = {nodeData.cat_id}
        />
      </div>
    )
  }
}

class SamplePage extends React.Component {
  state = {
    data: [{
      name: '.......Loading sale structure',
      children: [],
    }],
    ready: false
  }

  componentDidMount() {
    const dimensions = this.treeContainer.getBoundingClientRect();
    this.setState({
      translate: {
        x: dimensions.width / 7,
        y: dimensions.height / 3
      }
    });
    this.props.getStructure()
    this.props.getCategories()
    this.props.getReps()
    }
    componentWillReceiveProps(nextProps){
      if(nextProps.data.length){
        this.setState({ data : nextProps.data, ready : true})
      }
      if(nextProps.refresh){
        this.props.getStructure()
        this.props.getCategories()
        this.props.getReps()
          }
    }
  render() {
    return (
      <div className="app-wrapper">
        <ContainerHeader match={this.props.match} title={<IntlMessages id="pages.samplePage"/>}/>
        <InfoView/>
        <div style={containerStyles} ref={tc => (this.treeContainer = tc)}>
  
          <Tree 
          translate={this.state.translate}
          data={this.state.data}
          collapsible={false}
          pathFunc = "diagonal"
          ref={t => (this.tree = t)}
          // onLinkClick={() => {console.log("link clicked")}} 
          allowForeignObjects
          nodeLabelComponent={{
            render: <NodeLabel className='myLabelComponentInSvg' tree={this.tree} addChild = {this.props.addChild} editChild = {this.props.editChild} addCategory = {this.props.addCategory} editCategory = {this.props.editCategory} salesreps = {this.props.salesrepdata} attachRep={this.props.attachRep} history={this.props.history}/>,
            foreignObjectWrapper: {
              y: -15,
              x: -15,
              width : 150
            }
          }}
          />
        
        </div>
      </div>
    );
  }
}


const mapDispatchToProps = dispatch => {
  return {
    getStructure: () => dispatch(getStructure()),
    addChild: (data) => dispatch(addChild(data)),
    editChild: (id, title) => dispatch(editChild(id, title)),
    getCategories: () => dispatch(getCategories()),
    addCategory: (data) => dispatch(addCategory(data)),
    editCategory: (id, title) => dispatch(editCategory(id, title)),
    getReps: () => dispatch(getReps()),
    attachRep: (id, structure_id) => dispatch(attachRep(id, structure_id)),
  }
}
const mapStateToProps = ({salestructure, salesreps, router}) => {
  const {data, refresh} = salestructure;
  const {salesrepdata} = salesreps;
  return {data, refresh, salesrepdata, router}
};

export default connect(mapStateToProps, mapDispatchToProps)(SamplePage);