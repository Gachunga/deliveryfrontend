import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';
import asyncComponent from '../../../util/asyncComponent';

const Dashboard = ({match}) => (
  <div className="app-wrapper">
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/tasks`}/>
      <Route path={`${match.url}/tasks`} component={asyncComponent(() => import('../tasks'))}/>
      <Route component={asyncComponent(() => import('components/Error404'))}/>
    </Switch>
  </div>
);

export default Dashboard;