import React from 'react';
import { compose, withProps, lifecycle,withHandlers, withState } from "recompose";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  Polyline,
  DirectionsRenderer
} from "react-google-maps";

import { APIKEY } from "../../../constants/ActionTypes";

const MyMapComponent = compose(
  withProps({
    googleMapURL:
      "https://maps.googleapis.com/maps/api/js?key="+APIKEY+"&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />
  }),
  // withScriptjs,
  withGoogleMap,
  lifecycle({
    componentWillReceiveProps(nextProps) {
      console.log(nextProps)
      if(!(nextProps.pickset&&nextProps.deliveryset)){
        return
      }
      const DirectionsService = new window.google.maps.DirectionsService();
      const pick = nextProps.task.pickup[0]
      const deliv = nextProps.task.delivery[0]
      DirectionsService.route({
        origin: new window.google.maps.LatLng(pick.picklocation.latLng.lat, pick.picklocation.latLng.lng),
        destination: new window.google.maps.LatLng(deliv.deliverylocation.latLng.lat, deliv.deliverylocation.latLng.lng),
        travelMode: window.google.maps.TravelMode.DRIVING,
      }, (result, status) => {
        if (status === window.google.maps.DirectionsStatus.OK) {
          this.setState({
            directions: result,
          });
        } else {
          console.error(`error fetching directions ${result}`);
        }
      });
    }
  })
)((props) => {
  const center = { lat: props.pickset ? props.picklocation.latLng.lat : -1.13, lng: props.pickset ? props.picklocation.latLng.lng :36.97 }
  
  const pathCoordinates =  props.directions.map((direction) => {
    const obj = { lat: direction.latitude, lng: direction.longitude }
    return obj
      
    })
    console.log(pathCoordinates)
  return(
    <GoogleMap 
    defaultZoom={12} 
    defaultCenter={{ lat: -1.13, lng: 36.97 }}
    center={center}
    >
      {props.task.pickup.map((pick) => {
        return <Marker 
        position={{ lat: pick.picklocation.latLng.lat, lng: pick.picklocation.latLng.lng }}
        icon={{
          url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
        }} 
        />
      })
      }

    {props.task.delivery.map((deliv) => {
      return <Marker 
      position={{ lat: deliv.deliverylocation.latLng.lat, lng: deliv.deliverylocation.latLng.lng }}
      icon={{
        url: "http://maps.google.com/mapfiles/ms/icons/red-dot.png"
      }} 
      />})
      }
      {pathCoordinates.length&&<Polyline
      path={pathCoordinates}
      geodesic={true}
      options={{
          strokeColor: "#ff2527",
          strokeOpacity: 0.75,
          strokeWeight: 2,
          
      }}></Polyline>
      }
    </GoogleMap>
)
});

export default MyMapComponent;
