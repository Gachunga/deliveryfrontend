import React from 'react';
import ContainerHeader from 'components/ContainerHeader/index';
import IntlMessages from 'util/IntlMessages';
import CardBox from 'components/CardBox/index';
import MaterialTable from "material-table";
import axios from 'util/Api';
import { forwardRef } from 'react';
import {NotificationContainer, NotificationManager} from 'react-notifications';

import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import Refresh from '@material-ui/icons/Refresh';
import { push } from 'connected-react-router'
import CustomerDialog from './CustomerDialog';
import Button from '@material-ui/core/Button';
import { APIKEY, BASE_URL } from 'constants/ActionTypes';
import MapComponent from './Map';
import moment from 'moment';

const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Refresh: forwardRef((props, ref) => <Refresh {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
  };

class Customers extends React.Component {
  state = {
    anchorEl: undefined,
    open: false,
    opendialog : false,
    edit : false,
    opencustomerdialog : false,
    opensalesreps : false
  };
  tableRef = React.createRef();
  componentDidMount() {
    // this.loadJS('https://maps.googleapis.com/maps/api/js?key='+APIKEY+'&v=3.exp&libraries=geometry,drawing,places')
  }
  loadJS(src) {
    var ref = window.document.getElementsByTagName("script")[0];
    console.log(ref)
    var script = window.document.createElement("script");
    script.src = src;
    script.async = true;
    ref.parentNode.insertBefore(script, ref);
  }
  notify(type){
    if(type=="success"){
     NotificationManager.success(<IntlMessages id="notification.successMessage"/>); 
    }else if(type=="info"){
      NotificationManager.info(<IntlMessages id="notification.infoMsg"/>);
    }
    else if(type=="warning"){
      NotificationManager.warning(<IntlMessages id="notification.warningMessage"/>, <IntlMessages
            id="notification.closeAfter3000ms"/>, 3000);
    }
    else if(type=="error"){
      NotificationManager.error(<IntlMessages id="notification.errorMessage"/>, <IntlMessages
            id="notification.clickMe"/>, 5000, () => {
            alert('callback');
          });
    }
  }
  createNotification = (type) => {
    return () => {
      switch (type) {
        case 'info':
          NotificationManager.info(<IntlMessages id="notification.infoMsg"/>);
          break;
        case 'success':
          NotificationManager.success(<IntlMessages id="notification.successMessage"/>, <IntlMessages
            id="notification.titleHere"/>);
          break;
        case 'warning':
          NotificationManager.warning(<IntlMessages id="notification.warningMessage"/>, <IntlMessages
            id="notification.closeAfter3000ms"/>, 3000);
          break;
        case 'error':
          NotificationManager.error(<IntlMessages id="notification.errorMessage"/>, <IntlMessages
            id="notification.clickMe"/>, 5000, () => {
            alert('callback');
          });
          break;
        default:
          NotificationManager.info(<IntlMessages id="notification.infoMsg"/>);
      }
    };
  };
  onCustomerDialogClose = (value) => {
    this.setState({opencustomerdialog : false})
  }
  refresh= () => {
    this.tableRef.current.onQueryChange();
  }
  render() {
    console.log(window.google)
    return (
      <div>
      <div className="app-wrapper">
        <div style={{ maxWidth: '100%' }}>
        <MaterialTable
          tableRef={this.tableRef}
          options={{
            exportButton: true,
            pageSize:10,
            pageSizeOptions : [10],
            exportAllData: false
          }}
          
          icons={tableIcons}
          columns={[
            {
              field: 'agent',
              title: 'Agent',
              render: (rowData) => {
                return rowData.agent.name
              } 
            },
            { title: 'status', field: 'status' },
            {
              field: 'pickupname',
              title: 'Pick Up Name',
              render: (rowData) => {
                return rowData.pickup[0].pickupname
              } 
            },
            {
              field: 'picklocation',
              title: 'Pick Up Location',
              render: (rowData) => {
                return rowData.pickup[0].picklocation.address
              } 
            },
            {
              field: 'pickphone',
              title: 'Pick Up Phone',
              render: (rowData) => {
                return rowData.pickup[0].pickphone
              } 
            },
            {
              field: 'pickbefore',
              title: 'Pick Up Before',
              render: (rowData) => {
                return moment(rowData.pickup[0].pickbefore).format("dddd YYYY Do, h:mm:ss a")
              } 
            },
            {
              field: 'accepted_at',
              title: 'Accepted at',
              render: (rowData) => {
                return  rowData.pickup[0].accepted_at ? moment(rowData.pickup[0].accepted_at).format("dddd YYYY Do, h:mm:ss a") : ""
              } 
            },
            {
              field: 'started_at',
              title: 'Started at',
              render: (rowData) => {
                return  rowData.pickup[0].started_at ? moment(rowData.pickup[0].started_at).format("dddd YYYY Do, h:mm:ss a") : ""
              } 
            },
            {
              field: 'completed_at',
              title: 'Completed at',
              render: (rowData) => {
                return  rowData.pickup[0].completed_at ? moment(rowData.pickup[0].completed_at).format("dddd YYYY Do, h:mm:ss a") : ""
              } 
            },
            {
              field: 'pickup',
              title: 'Pick Up Status',
              render: (rowData) => {
                return rowData.pickup[0].status
              } 
            },
            {
              field: 'note',
              title: 'Pick Up Note',
              render: (rowData) => {
                return rowData.pickup[0].note
              } 
            },
            {
              field: 'url',
              title: 'Image',
              render: rowData => <img src={BASE_URL+'tasks/file/'+rowData.pickup[0].img_file} style={{width: 100, borderRadius: '0%'}}/>
            },
            {
              field: 'url',
              title: 'Signature',
              render: rowData => <img src={BASE_URL+'tasks/file/'+rowData.pickup[0].sign_file} style={{width: 100, borderRadius: '0%'}}/>
            },
            {
              field: 'deliveryname',
              title: 'Delivery Name',
              render: (rowData) => {
                return rowData.delivery[0].deliveryname
              } 
            },
            {
              field: 'deliverylocation',
              title: 'Delivery Location',
              render: (rowData) => {
                return rowData.delivery[0].deliverylocation.address
              } 
            },
            {
              field: 'deliveryphone',
              title: 'Delivery Phone',
              render: (rowData) => {
                return rowData.delivery[0].deliveryphone
              } 
            },
            {
              field: 'deliveryphone',
              title: 'Delivery Phone',
              render: (rowData) => {
                return moment(rowData.delivery[0].pickbefore).format("dddd YYYY Do, h:mm:ss a")
              } 
            },
            {
              field: 'accepted_at',
              title: 'Accepted at',
              render: (rowData) => {
                return  rowData.delivery[0].accepted_at ? moment(rowData.delivery[0].accepted_at).format("dddd YYYY Do, h:mm:ss a") : ""
              } 
            },
            {
              field: 'started_at',
              title: 'Started at',
              render: (rowData) => {
                return  rowData.delivery[0].accepted_at ? moment(rowData.delivery[0].started_at).format("dddd YYYY Do, h:mm:ss a") : ""
              } 
            },
            {
              field: 'completed_at',
              title: 'Completed at',
              render: (rowData) => {
                return  rowData.delivery[0].completed_at ? moment(rowData.delivery[0].completed_at).format("dddd YYYY Do, h:mm:ss a") : ""
              } 
            },
            {
              field: 'delivery',
              title: 'Delivery Status',
              render: (rowData) => {
                return rowData.delivery[0].status
              } 
            },
            {
              field: 'note',
              title: 'Delivery Note',
              render: (rowData) => {
                return rowData.delivery[0].note
              } 
            },
            {
              field: 'url',
              title: 'Image',
              render: rowData => <img src={BASE_URL+'tasks/file/'+rowData.delivery[0].img_file} style={{width: 100, borderRadius: '0%'}}/>
            },
            {
              field: 'url',
              title: 'Signature',
              render: rowData => <img src={BASE_URL+'tasks/file/'+rowData.delivery[0].sign_file} style={{width: 100, borderRadius: '0%'}}/>
            },
            {
              field: 'created_at',
              title: 'Created At',
              render: (rowData) => {
                return moment(rowData.created_at).format("dddd YYYY Do, h:mm:ss a")
              } 
            },
            {
              field: 'updated_at',
              title: 'Updated At',
              render: (rowData) => {
                return moment(rowData.updated_at).format("dddd YYYY Do, h:mm:ss a")
              } 
            },
            
          ]}
          detailPanel={[
            {
              tooltip: 'Show Name',
              render: rowData => {
                console.log(rowData.routeCoordinates)
                return (
                  <div
                    style={{
                      fontSize: 100,
                      textAlign: 'center',
                      color: 'white',
                      backgroundColor: '#43A047',
                    }}
                  >
                   <MapComponent task={rowData} directions={rowData.routeCoordinates}/>
                  </div>
                )
              },
            },
          ]}
          onRowClick={(event, rowData, togglePanel) => togglePanel()}
          data={query =>
            new Promise((resolve, reject) => {
              let url = 'tasks/indexpaginated?'
                url += '&page=' + (query.page + 1)
                axios.get(url,{ handlerEnabled: true }
                ).then(({data}) => {
                  console.log("fetchTasks: ", data);
                  if (data.status=200) {
                    resolve({
                      data: data.data.data,
                      page: data.data.page-1,
                      totalCount: data.data.total,
                  });
                  } else {
                    this.notify("error")
                    console.log("error : "+data)
                  }
                }).catch((error) => {
                  const response = []
                  this.notify("error")
                  console.log("Error****:", error.message);
                });
            })
        }
          title="Tasks"
          
        />
      </div>
      </div>
      <NotificationContainer/>
      </div>
    );
  }
}

export default Customers;