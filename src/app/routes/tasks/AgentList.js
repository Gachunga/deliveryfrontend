import React, { Component } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import Avatar from "@material-ui/core/Avatar";
import { Badge } from "reactstrap";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import IconButton from '@material-ui/core/IconButton';

class AgentList extends Component {
  state = {
    checked: [],
    agents : [],
    showmore : false
  };
  componentDidMount(){
    
  }
  handleToggle = (event, agent) => {
    this.setState({showmore : !this.state.showmore, agent : agent})
  }

  render() {
    const {agents} = this.props;
    return (
      <List>
        { agents.map(agent =>
          !this.state.showmore ?<ListItem button key={agent._id} onClick={event => this.handleToggle(event, agent)}>
            <ListItemAvatar>
            <Avatar className="bg-primary size-60" style={{margin : 20}}><h1 className="m-0 text-white">{agent.name.substring(0, 2)}</h1></Avatar>
            </ListItemAvatar>
            <div style={{alignItems : "flex-column"}}>
            <ListItemText className="br-break" primary={agent.name} />
            <ListItemText className="br-break" primary={agent.phone} />
            </div>
            
            <Badge className="mr-4 mt-2 text-uppercase" color="primary" pill style={{marginLeft : 20}}>{agent.tasks.length}</Badge> tasks
            <ListItemSecondaryAction>
            <IconButton aria-label="more"
            onClick={event => this.handleToggle(event, agent)}>
              <ArrowForwardIosIcon/>
            </IconButton>
            </ListItemSecondaryAction>
          </ListItem> :
          <div style={{alignItems : "flex-column"}}>
          {this.state.agent&&
          <div>
          <ListItem>
            <ListItemText className="br-break" primary="Name" />
            <ListItemText className="br-break" primary={this.state.agent.name} />
          </ListItem>
          <ListItem>
            <ListItemText className="br-break" primary="Contact" />
            <ListItemText className="br-break" primary={this.state.agent.phone} />
          </ListItem>
          <ListItem>
            <ListItemText className="br-break" primary="Last Location" />
            <ListItemText className="br-break" primary="" />
          </ListItem>
          <ListItem>
            <ListItemText className="br-break" primary="Email" />
            <ListItemText className="br-break" primary={this.state.agent.email} />
          </ListItem>
          <ListItem>
            <ListItemText className="br-break" primary="Active" />
            <ListItemText className="br-break" primary={this.state.agent.active ? "YES" : "NO"} />
          </ListItem>
          <ListItem>
          <ListItemSecondaryAction>
            <IconButton aria-label="more"
            onClick={this.handleToggle}>
              <ArrowBackIosIcon/>
            </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
          </div>}
        </div>
        ) 
        
        }
      </List>
    );
  }
}

export default AgentList;