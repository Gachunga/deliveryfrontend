import React, {Component} from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Button from '@material-ui/core/Button';
import CloseIcon from '@material-ui/icons/Close';
import Timeline from './timeLine/routes/defaultWithIcon';
import AgentList from './AgentList';

function TabContainer(props) {
  return (
    <div style={{padding: 20}}>
      {props.children}
    </div>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

class AgentTabs extends Component {
  state = {
    value: 1,
  };

  handleChange = (event, value) => {
    this.setState({value});
  };

  render() {
    const {value} = this.state;
    const data = this.props.agentsdata
    console.log(data)
    const free = data.filter((currentValue, index) => {
      if(currentValue.active&&!currentValue.engaged){
        return currentValue
      }
    });
    const busy = data.filter((currentValue, index) => {
      if(currentValue.active&&currentValue.engaged){
        return currentValue
      }
    });
    const inactive = data.filter((currentValue, index) => {
      if(!currentValue.active){
        return currentValue
      }
    });
    return (
      <div>
        <AppBar className="bg-primary" position="static">
          <Tabs value={value} onChange={this.handleChange} variant="scrollable" scrollButtons="on">
            <Tab className="tab" label="Free"/>
            <Tab className="tab" label="Busy"/>
            <Tab className="tab" label="Inactive"/>
            <Button
              variant="contained"
              color="primary"
              startIcon={<CloseIcon />}
              onClick={() => {this.props.openLeftPanel(false)}}
              >
              </Button>
            </Tabs>
         
        </AppBar>
        {value === 0 &&
        <TabContainer>
         <AgentList agents={free}></AgentList>
        </TabContainer>}
        {value === 1 &&
        <TabContainer>
          <AgentList agents={busy}></AgentList>
        </TabContainer>}
        {value === 2 &&
        <TabContainer>
          <AgentList agents={inactive}></AgentList>
        </TabContainer>}
      </div>
    );
  }
}

export default AgentTabs;