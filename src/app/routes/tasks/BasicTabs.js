import React, {Component} from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Button from '@material-ui/core/Button';
import CloseIcon from '@material-ui/icons/Close';
import Timeline from './timeLine/routes/defaultWithIcon';
import moment from 'moment';
import {DatePicker} from 'material-ui-pickers';
import TextField from '@material-ui/core/TextField';
import { withStyles } from "@material-ui/core/styles";

const styles = {
  root: {
    background: "white"
  },
  input: {
    color: "blue"
  }
};


function TabContainer(props) {
  return (
    <div style={{padding: 20}}>
      {props.children}
    </div>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

class BasicTabs extends Component {
  state = {
    value: 1,
    selectedDate: moment(),
  };

  handleChange = (event, value) => {
    this.setState({value});
  };
  handleDateChange = (date) => {
    this.setState({selectedDate: date});
  };

  render() {
    const {value} = this.state;
    const data = this.props.tasksdata
    const unassigned = data.filter((currentValue, index) => {
      if(currentValue.agent_id==null){
        return currentValue
      }
    });
    const assigned = data.filter((currentValue, index) => {
      if(currentValue.agent_id!=null&&currentValue.status!="successful"){
        return currentValue
      }
    });
    const completed = data.filter((currentValue, index) => {
      if(currentValue.status=="successful"){
        return currentValue
      }
    });
    const { classes, selectedDate, handleDateChange } = this.props;
    return (
      <div>
        <AppBar className="bg-primary" position="static">
          <div key="basic_day" className="picker">
                <DatePicker
                  fullWidth
                  value={selectedDate}
                  onChange={handleDateChange}
                  animateYearScrolling={false}
                  TextFieldComponent={props => (
                    <TextField
                      {...props}
                      value={selectedDate}
                      variant="outlined"
                      // onChange={event => handleDateChange(event.target.value)}
                      className={classes.root}
                      InputProps={{
                        className: classes.input
                      }}
                    />
                  )}
                  leftArrowIcon={<i className="zmdi zmdi-arrow-back"/>}
                  rightArrowIcon={<i className="zmdi zmdi-arrow-forward"/>}
                />
              </div>
          <Tabs value={value} onChange={this.handleChange} variant="scrollable" scrollButtons="on">
            <Tab className="tab" label="Unassigned"/>
            <Tab className="tab" label="Assigned"/>
            <Tab className="tab" label="Completed"/>
            <Button
              variant="contained"
              color="primary"
              startIcon={<CloseIcon />}
              onClick={() => {this.props.openLeftPanel(false)}}
              >
              </Button>
              
            </Tabs>
         
        </AppBar>
        {value === 0 &&
        <TabContainer>
          <Timeline data={unassigned} setMapLineFromTasks={this.props.setMapLineFromTasks}/>
        </TabContainer>}
        {value === 1 &&
        <TabContainer>
          <Timeline data={assigned} setMapLineFromTasks={this.props.setMapLineFromTasks}/>
        </TabContainer>}
        {value === 2 &&
        <TabContainer>
          <Timeline data={completed} setMapLineFromTasks={this.props.setMapLineFromTasks}/>
        </TabContainer>}
      </div>
    );
  }
}
BasicTabs.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(BasicTabs);