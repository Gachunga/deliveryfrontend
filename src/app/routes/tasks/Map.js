import React from 'react';
import { compose, withProps, lifecycle,withHandlers, withState } from "recompose";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  Polyline,
  DirectionsRenderer,
} from "react-google-maps";

import { APIKEY } from "../../../constants/ActionTypes";

const MyMapComponent = compose(
  withProps({
    googleMapURL:
      "https://maps.googleapis.com/maps/api/js?key="+APIKEY+"&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />
  }),
  // withScriptjs,
  withGoogleMap,
  lifecycle({
    componentWillReceiveProps(nextProps) {
      console.log(nextProps)
      if(!(nextProps.pickset&&nextProps.deliveryset)){
        return
      }
      const DirectionsService = new window.google.maps.DirectionsService();

      DirectionsService.route({
        origin: new window.google.maps.LatLng(nextProps.pickset ? nextProps.picklocation.latLng.lat : -1.13, nextProps.pickset ? nextProps.picklocation.latLng.lng :36.97),
        destination: new window.google.maps.LatLng(nextProps.deliveryset ? nextProps.deliverylocation.latLng.lat : -1.13, nextProps.deliveryset ? nextProps.deliverylocation.latLng.lng :36.97),
        travelMode: window.google.maps.TravelMode.DRIVING,
      }, (result, status) => {
        if (status === window.google.maps.DirectionsStatus.OK) {
          this.setState({
            directions: result,
          });
        } else {
          console.error(`error fetching directions ${result}`);
        }
      });
    }
  })
)((props) => {
  const center = { lat: props.pickset ? props.picklocation.latLng.lat : -1.13, lng: props.pickset ? props.picklocation.latLng.lng :36.97 }
  return(
    <GoogleMap 
    defaultZoom={12} 
    defaultCenter={{ lat: -1.13, lng: 36.97 }}
    center={center}
    >
      {props.pickset&&
      <Marker 
      position={{ lat: props.pickset ? props.picklocation.latLng.lat : -1.13, lng: props.pickset ? props.picklocation.latLng.lng :36.97 }}
      icon={{
        url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
      }} 
      />}

      {props.deliveryset&&
      <Marker 
      position={{ lat: props.deliveryset ? props.deliverylocation.latLng.lat : -1.13, lng: props.deliveryset ? props.deliverylocation.latLng.lng :36.97 }}
      icon={{
        url: "http://maps.google.com/mapfiles/ms/icons/red-dot.png"
      }} 
      />}

      {props.directions && <DirectionsRenderer directions={props.directions} />}
    </GoogleMap>
)
});

export default MyMapComponent;
