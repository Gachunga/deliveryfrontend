import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import SearchBox from 'components/SearchBox';
import AddIcon from '@material-ui/icons/Add';
import Avatar from '@material-ui/core/Avatar';
import {Dropdown, DropdownMenu, DropdownToggle} from 'reactstrap';
import ListIcon from '@material-ui/icons/List';
import ListAltIcon from '@material-ui/icons/ListAlt';

class FullFeatured extends React.Component {

  onSearchBoxSelect = () => {
    this.setState({
      searchBox: !this.state.searchBox
    })
  };
  handleRequestClose = () => {
    this.setState({mailNotification: false, appNotification: false, searchBox: false});
  };

  constructor() {
    super();
    this.state = {
      searchBox: false,
      searchText: '',
    }
  }

  updateSearchText(evt) {
    this.setState({
      searchText: evt.target.value,
    });
  }

  render() {
    return (
      <AppBar className="app-main-header jr-border-radius" position="static">
        <Toolbar>
        <Button
        variant="contained"
        color="secondary"
        startIcon={<AddIcon />}
        onClick={() => {this.props.openLeftPanel(true)}}
        >
          Create Task
        </Button>
        <div>

        </div>
        <Button
        style={{padding: '6px 12px', marginLeft : 5}}
        variant="contained"
        color="warning"
        startIcon={<ListIcon />}
        onClick={() => {this.props.openLeftTaskPanel(true)}}
        >
          Task List
        </Button>  
        <Button
        style={{padding: '6px 12px', marginLeft : 5}}
        variant="contained"
        color="secondary"
        startIcon={<ListAltIcon />}
        onClick={() => {this.props.openRightPanel(true)}}
        >
          Agents
        </Button>  
          
          <div className="d-inline-block d-sm-none list-inline-item">
            <Dropdown
              className="quick-menu nav-searchbox"
              isOpen={this.state.searchBox}
              toggle={this.onSearchBoxSelect.bind(this)}>

              <DropdownToggle
                className="d-inline-block"
                tag="span"
                data-toggle="dropdown">
                <IconButton className="icon-btn size-30">
                  <i className="zmdi zmdi-search zmdi-hc-fw"/>
                </IconButton>
              </DropdownToggle>

              <DropdownMenu right className="p-0">
                <SearchBox styleName="search-dropdown" placeholder=""
                           onChange={this.updateSearchText.bind(this)}
                           value={this.state.searchText}/>
              </DropdownMenu>
            </Dropdown>
          </div>

          {/* <ul className="header-notifications list-inline ml-3 d-none d-sm-block">

            <li className="list-inline-item">
              <i className="zmdi zmdi-notifications-active zmdi-hc-lg zmdi-hc-fw"/>
            </li>
            <li className="list-inline-item">
              <i className="zmdi zmdi-comment-alt-text zmdi-hc-lg zmdi-hc-fw"/>
            </li>
          </ul>

          <Avatar className="ml-3 ml-lg-5 d-none d-sm-block" alt="Remy Sharp"
                  src='https://via.placeholder.com/120x120'/> */}
        </Toolbar>
      </AppBar>
    );
  }
}

export default FullFeatured;