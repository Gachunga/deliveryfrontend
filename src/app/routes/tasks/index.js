import React from 'react';
import ContainerHeader from 'components/ContainerHeader';
import IntlMessages from 'util/IntlMessages';
import { render } from 'react-dom';
import Modal from 'react-modal';
import SlidingPane from 'react-sliding-pane';
import 'react-sliding-pane/dist/react-sliding-pane.css';
import {connect} from 'react-redux';
import { HORIZONTAL_NAVIGATION } from 'constants/ActionTypes';
import MapComponent from './Map';
import ExpansionPanel from './simple';
import Bar from './fullyFeatured';
import IconButton from '@material-ui/core/IconButton';
import { Clear } from '@material-ui/icons'
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import MenuIcon from "@material-ui/icons/Menu";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import SaveIcon from "@material-ui/icons/Save";
import EditIcon from "@material-ui/icons/Edit";
import BasicTabs from "./BasicTabs";
import AgentTabs from "./AgentTabs";
import { getTasks, getAgents } from "../../../actions/tasks";
import connection from './lib/socket';
import moment from 'moment';

let subscription;

const styles2 = {
  // these buttons will be aligned to right side of abbBar
  toolbarButtons: {
    marginLeft: "auto",
    marginRight: -12
  },
  menuButton: {
    marginRight: 20,
    marginLeft: -12
  }
};

const Demo = withStyles(styles2)(({ classes, openLeftPanel }) => (
  <AppBar position="static">
    <Toolbar>
      <Typography variant="title" color="inherit">
        Add New Task
      </Typography>
      <span className={classes.toolbarButtons}>
        <IconButton color="inherit" aria-label="Edit">
          <EditIcon />
        </IconButton>
        <IconButton color="inherit" aria-label="Save">
          <SaveIcon />
        </IconButton>
        <IconButton color="inherit" aria-label="Clear"
        onClick={() => {openLeftPanel(false)}}>
          <Clear />
        </IconButton>
      </span>
    </Toolbar>
  </AppBar>
));

const BarRight = withStyles(styles2)(({ classes, openRightPanel }) => (
  <AppBar position="static">
    <Toolbar>
      <Typography variant="title" color="inherit">
        Agents
      </Typography>
      <span className={classes.toolbarButtons}>
        <IconButton color="inherit" aria-label="Clear"
        onClick={() => {openRightPanel(false)}}>
          <Clear />
        </IconButton>
      </span>
    </Toolbar>
  </AppBar>
));

const styles = {
  // these buttons will be aligned to right side of abbBar
  toolbarButtons: {
    marginLeft: "auto",
    marginRight: -12
  },
  menuButton: {
    marginRight: 20,
    marginLeft: -12
  }
};

class TaskPage extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          isPaneOpen: false,
          isPane2Open: false,
          isPaneOpenRight : false,
          isPaneOpenLeft: false,
          picklocation : {},
          deliverlocation:{},
          pickset: false,
          isPaneOpenLeftTasks : true,
          deliveryset : false,
          selectedDate : moment().format('LLLL')
      };
  }

  componentDidMount() {
    // connection.connect();

    // storing the subscription in the global variable
    // passing the incoming data handler fn as a second argument
    // subscription = connection.subscribe(`tasks:task`, this.handleTaskChange);
    // console.log("subscription", subscription)
      Modal.setAppElement(this.el);
      const payload = {date : this.state.selectedDate}
      this.props.getTasks(payload)
      this.props.getAgents()
  }
  componentWillUnmount () {
    // subscription.close();
  }

  handleTaskChange = message => {
    const payload = {date : this.state.selectedDate}
    this.props.getTasks(payload)
    
    console.log(message)
    const { type, data } = message;

    // you could handle various types here, like deleting or editing a message
    switch (type) {
      case 'task:accepted':
        console.log(data)
        // this.setState(prevState => ({
        //   messages: [...prevState.messages, data]
        // }));
        break;
      default:
    }
  };
  openLeftPanel = (open) => {
    this.setState({ isPaneOpenLeft: open })
  }
  openLeftTaskPanel = (open) => {
    this.setState({ isPaneOpenLeftTasks: open })
  }
  submitted = () => {
    this.setState({ isPaneOpenLeft: false })
    this.setState({ isPaneOpenLeftTasks: true })
  }
  openRightPanel = (open) => {
    this.setState({ isPaneOpenRight: open })
  }
  setPickLocation = (location) => {
    this.setState({ picklocation: location, pickset : true})
  }
  setDeliveryLocation = (location) => {
    this.setState({ deliverylocation: location, deliveryset : true })
  }
  setMapLineFromTasks = (value) => {
    this.setState({ 
      deliverylocation: value.pickup[0].picklocation, 
      deliveryset : true,
      picklocation: value.delivery[0].deliverylocation, 
      pickset : true 
    })
  }
  handleDateChange = (date) => {
    console.log(date)
    this.setState({selectedDate: moment(date).format('LLLL')});
    const payload = {date : moment(date).format('LLLL')}
    console.log(payload)
    this.props.getTasks(payload)
  };
  render() {
      return <div ref={ref => this.el = ref}>
         <SlidingPane
              closeIcon={<IconButton className={`jr-menu-icon mr-3`} aria-label="Close"
              onClick={() => {this.setState({ isPaneOpenLeftTasks: false })}}>
              <span className="close-icon" />
            </IconButton>}
              isOpen={ this.state.isPaneOpenLeftTasks }
              title='Hey, it is optional pane title.  I can be React component too.'
              from='left'
              width='50%'
              top='40'
              onRequestClose={ () => this.setState({ isPaneOpenLeftTasks: false }) }>
              <div>
               <BasicTabs setMapLineFromTasks={this.setMapLineFromTasks} openLeftPanel={this.openLeftTaskPanel} openLeftTaskPanel={this.openLeftTaskPanel} tasksdata={this.props.tasksdata} handleDateChange={this.handleDateChange} selectedDate={this.state.selectedDate}/>
              </div>
          </SlidingPane>
         
          <div className="app-wrapper">
          <div style={{width : this.state.isPaneOpenLeft||this.state.isPaneOpenLeftTasks ? '50%' : '100%', marginLeft: this.state.isPaneOpenLeft||this.state.isPaneOpenLeftTasks ? '50%' : 0}}>
          <Bar openLeftPanel={this.openLeftPanel} openRightPanel={this.openRightPanel} openLeftTaskPanel={this.openLeftTaskPanel}/>
          <MapComponent picklocation={this.state.picklocation} deliverylocation={this.state.deliverylocation} pickset = {this.state.pickset} deliveryset={this.state.deliveryset}></MapComponent>
          </div>
          </div>
          <SlidingPane
              closeIcon={<IconButton className={`jr-menu-icon mr-3`} aria-label="Close"
              onClick={() => {this.setState({ isPaneOpenLeft: false })}}>
              <span className="close-icon" />
            </IconButton>}
              isOpen={ this.state.isPaneOpenLeft }
              title='Hey, it is optional pane title.  I can be React component too.'
              from='left'
              width='50%'
              top='40'
              onRequestClose={ () => this.setState({ isPaneOpenLeft: false }) }>
              <div>
                <Demo hello={"hello"} openLeftPanel={this.openLeftPanel}></Demo>
                <ExpansionPanel selectedDate={this.state.selectedDate} setPickLocation={this.setPickLocation} setDeliveryLocation={this.setDeliveryLocation} submitted ={this.submitted}/>
              </div>
          </SlidingPane>
          <SlidingPane
              closeIcon={<IconButton className={`jr-menu-icon mr-3`} aria-label="Close"
              onClick={() => {this.setState({ isPaneOpenRight: false })}}>
              <span className="close-icon" />
            </IconButton>}
              isOpen={ this.state.isPaneOpenRight }
              title='Hey, it is optional pane title.  I can be React component too.'
              from='right'
              width='40%'
              top='40'
              onRequestClose={ () => this.setState({ isPaneOpenRight: false }) }>
              <div>
              <BarRight hello={"hello"} openRightPanel={this.openRightPanel}></BarRight>
              <AgentTabs setMapLineFromTasks={this.setMapLineFromTasks} openLeftPanel={this.openLeftTaskPanel} openLeftTaskPanel={this.openLeftTaskPanel} agentsdata={this.props.agentsdata}/>
              </div>
          </SlidingPane>
      </div>;
  }
}
const mapDispatchToProps = dispatch => {
  return {
    getTasks: (payload) => dispatch(getTasks(payload)),
    getAgents: () => dispatch(getAgents())
  }
};
const mapStateToProps = ({settings, tasks}) => {
  const {navCollapsed, drawerType, width, navigationStyle} = settings;
  const {tasksdata, agentsdata} = tasks;
  return {navCollapsed, drawerType, width, navigationStyle, tasksdata, agentsdata}
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskPage);