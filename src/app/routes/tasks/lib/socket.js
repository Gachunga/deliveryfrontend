import Ws from '@adonisjs/websocket-client';

import { getSocketProtocol } from '../../../../util/data';
import { BASE_WEB_SOCKET_URL } from '../../../../constants/ActionTypes'

export class SocketConnection {
  connect () {
    this.ws = Ws(`${BASE_WEB_SOCKET_URL}`)
    // .withApiToken(token)
      .connect();

    this.ws.on('open', () => {
      console.log('Connection initialized')
    });

    this.ws.on('close', () => {
      console.log('Connection closed')
    });

    return this
  }

  subscribe (channel, handler) {
    if (!this.ws) {
      setTimeout(() => this.subscribe(channel), 1000)
    } else {
      const result = this.ws.subscribe(channel);

      result.on('message', message => {
        console.log('Incoming', message);
        handler(message)
      });

      result.on('error', (error) => {
        console.error(error)
      });

      return result
    }
  }
}

export default new SocketConnection()
