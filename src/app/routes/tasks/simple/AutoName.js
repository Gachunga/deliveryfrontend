import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Person from '@material-ui/icons/Person';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import parse from 'autosuggest-highlight/parse';
import throttle from 'lodash/throttle';
import { BASE_URL } from '../../../../constants/ActionTypes';
import CircularProgress from '@material-ui/core/CircularProgress';
import axios from 'util/Api'

function loadScript(src, position, id) {
  if (!position) {
    return;
  }

  const script = document.createElement('script');
  script.setAttribute('async', '');
  script.setAttribute('id', id);
  script.src = src;
  position.appendChild(script);
}


const useStyles = makeStyles(theme => ({
  icon: {
    color: theme.palette.text.secondary,
    marginRight: theme.spacing(2),
  },
}));



export default function AutoName(props) {
  const classes = useStyles();
  const [inputValue, setInputValue] = React.useState('');
  const [options, setOptions] = React.useState([]);
  const loaded = React.useRef(false);
  const [open, setOpen] = React.useState(false);
  const loading = open && options.length === 0;
  
  if (typeof window !== 'undefined' && !loaded.current ) {
    
    loaded.current = true;
  }

  const handleChange = event => {
    setInputValue(event.target.value);
  };

  const onNameChange = (event, values) => {
    props.setName(values)
   }

  React.useEffect(() => {
    let active = true;
    
    if (inputValue === '') {
      setOptions([]);
      return undefined;
    }
    if (!loading) {
      return undefined;
    }
    (async () => {
      const response = await axios.get(BASE_URL+'customers/search/'+inputValue, { handlerEnabled: true });
      console.log(response.data)
      var customers = response.data
      customers = customers.data
      
      if (active) {
        customers.length ?
        setOptions(Object.keys(customers).map(key => customers[key])) : setOptions([]);
      }
    })();

    return () => {
      active = false;
    };
  }, [inputValue, loading]);

  React.useEffect(() => {
    if (!open) {
      setOptions([]);
    }
  }, [open]);

  return (
    <Autocomplete
      id="google-map-demo"
      // style={{ width: 300 }}
      open={open}
      onOpen={() => {
        setOpen(true);
      }}
      onClose={() => {
        setOpen(false);
      }}
      getOptionSelected={(option, value) => option.name === value.name}
      loading={loading}
      onChange={onNameChange}
      getOptionLabel={option => (typeof option === 'string' ? option : option.name)}
      filterOptions={x => x}
      options={options}
      autoComplete
      includeInputInList
      freeSolo
      disableOpenOnFocus={false}
      renderInput={ params => (
        <TextField
          {...params}
          label="Name"
          variant="outlined"
          fullWidth
          onChange={handleChange}
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
        />
      )}
      renderOption={option => {
        const matches = option.name;
        
        return (
          <Grid container alignItems="center">
            <Grid item>
              <Person className={classes.icon} />
            </Grid>
            <Grid item xs>
             
              <Typography variant="body2" color="textSecondary">
                {option.name}
              </Typography>
            </Grid>
          </Grid>
        );
      }}
    />
  );
}