import React, { Component } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import Avatar from "@material-ui/core/Avatar";
import { Badge } from "reactstrap";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import axios from 'util/Api'

class CheckBoxListControl extends Component {
  state = {
    checked: [],
    agents : [],
  };
  componentDidMount(){
    axios.get('agents',
      ).then(({data}) => {
        console.log("fetchAgents: ", data);
        if (data.status=200) {
          this.setState({agents : data.data.data})
        } else {
         console.log(data)
        }
      }).catch(function (error) {
        console.log("Error****:", error.message);
      });
  }
  handleToggle = (event, value) => {
    const {checked} = this.state;
    const currentIndex = checked.indexOf(value);
    
    if (currentIndex === -1) {
      this.setState({ checked: [value] });
      this.props.handleChange("agent_id", value)
    } else {
      this.setState({ checked: [] });
      this.props.handleChange("agent_id", "")
    }
    
  };

  render() {
    const {agents} = this.state;
    return (
      <List>
        {agents.map(agent =>
          <ListItem button key={agent._id} onClick={event => this.handleToggle(event, agent._id)}>
            <ListItemAvatar>
            <Avatar className="bg-primary size-40"><h1 className="m-0 text-white">{agent.name.substring(0, 1)}</h1></Avatar>
            </ListItemAvatar>
            <ListItemText className="br-break" primary={agent.name} />
            <Badge className="mr-4 mt-2 text-uppercase" color="success" pill>Agent</Badge>
            <ListItemSecondaryAction>
              <Checkbox color="primary"
                        checked={this.state.checked.indexOf(agent._id) !== -1}
                        onClick={event => this.handleToggle(event, agent._id)}
              />
            </ListItemSecondaryAction>
          </ListItem>,
        )}
      </List>
    );
  }
}

export default CheckBoxListControl;