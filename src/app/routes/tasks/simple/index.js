import React, { useState, Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {Card, CardBody, CardFooter, CardHeader, CardSubtitle, CardText} from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import SearchPlace from './searchPlace';
import Search from './Search';
import Name from './AutoName';
import {DateTimePicker} from 'material-ui-pickers';
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import CircularFab from './CircularFab';
import CheckboxListSecondary from './CheckboxListSecondary';
import InfoView from 'components/InfoView';
import axios from 'util/Api'
import { fetchStart, fetchError, fetchSuccess } from '../../../../actions/Common'
import { getTasks } from "../../../../actions/tasks";

const PickupCard = ({cardHeader, cardStyle, handleChange, setPickLocation}) => {
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [order, setOrder] = useState('');
  const [desc, setDesc] = useState('');
  const [name, setName] = useState('');
  const [address, setAddress] = useState('');
  
  const handleDateChange = (date) => {
    setSelectedDate(date);
    handleChange("pickbefore", date)
  };
  const handlePhoneChange = (phone) => {
    setPhone(phone);
    handleChange("pickphone", phone)
  };
  const handleEmailChange = (event) => {
    setEmail(event.target.value);
    handleChange("pickemail", event.target.value)
  };
  const handleOrderChange = (event) => {
    setOrder(event.target.value);
    handleChange("pickorderid", event.target.value)
  };
  const handleDescChange = (event) => {
    setDesc(event.target.value);
    handleChange("pickdesc", event.target.value)
  };
  const handlesetName = (name) => {
    if(name){
      setName(name._id);
      setEmail(name.email)
      setPhone(name.phone)
      handleChange("pickupname", name.name)
      handleChange("pickemail", name.email)
      handleChange("pickphone", name.phone)
      setPickLocation(name.address)
      handleLocationChange(name.address)
    }
  };
  const handleLocationChange = (location) => {
    handleChange("picklocation", location)
    setPickLocation(location)
    setAddress(location.address)
  }
  return (
    <Card className={`shadow border-0 ${cardStyle}`}>
      <CardHeader className="bg-primary text-white">Pick up details</CardHeader>
      <CardBody>
      <div style={{padding : 2}}>
          <form className="row" noValidate autoComplete="off">
              <div className="col-md-12 col-12" >
                <Name setName={handlesetName}/>
              </div>
              <div className="col-md-12 col-12" >
                <label>Phone</label>
              <PhoneInput
                  label="Phone"
                  country={'ke'}
                  value={phone}
                  onChange={(phone) => handlePhoneChange(phone)}
                />
              </div>
              <div className="col-md-6 col-12" >
                <TextField
                  id="email"
                  label="Email"
                  value={email}
                  onChange={handleEmailChange}
                  margin="normal"
                  fullWidth
                  variant="outlined"
                />
              </div>
              <div className="col-md-6 col-12" >
                <TextField
                  id="orderid"
                  label="Order ID"
                  value={order}
                  onChange={handleOrderChange}
                  margin="normal"
                  fullWidth
                  variant="outlined"
                />
              </div>
              {/* <div className="col-md-12 col-12" >
                <SearchPlace setLocation={handleLocationChange} address={address} text={"Pick Up Location"}/>
              </div> */}
              <div className="col-md-12 col-12" >
              <TextField
                  id="picklocation"
                  label="Pick Up Location"
                  value={address}
                  margin="normal"
                  fullWidth
                  variant="outlined"
                />
              </div>
              <div className="col-md-12 col-12" >
              <div key="datetime_default" className="picker">
              <label>Pick up before</label>
              <DateTimePicker
                fullWidth
                value={selectedDate}
                showTabs={false}
                variant="outlined"
                onChange={handleDateChange}
                leftArrowIcon={<i className="zmdi zmdi-arrow-back"/>}
                rightArrowIcon={<i className="zmdi zmdi-arrow-forward"/>}
              />
              </div>
              </div>
              <div className="col-md-12 col-12" >
                <TextField
                  id="Description"
                  label="desc"
                  value={desc}
                  onChange={handleDescChange}
                  margin="normal"
                  fullWidth
                  variant="outlined"
                />
              </div>
          </form>
        </div> 
      </CardBody>
      <CardFooter>Pick up details</CardFooter>
    </Card>
  );
};

const DeliveryCard = ({cardHeader, cardStyle, handleChange, setDeliveryLocation}) => {
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [order, setOrder] = useState('');
  const [desc, setDesc] = useState('');
  const [name, setName] = useState('');
  const [address, setAddress] = useState('');
  
  const handleDateChange = (date) => {
    setSelectedDate(date);
    handleChange("deliverbefore", date)
  };
  const handlePhoneChange = (phone) => {
    console.log(phone)
    setPhone(phone);
    handleChange("deliveryphone", phone)
  };
  const handleEmailChange = (event) => {
    setEmail(event.target.value);
    handleChange("deliveryemail", event.target.value)
  };
  const handleOrderChange = (event) => {
    setOrder(event.target.value);
    handleChange("deliveryorderid", event.target.value)
  };
  const handleDescChange = (event) => {
    setDesc(event.target.value);
    handleChange("deliverydesc", event.target.value)
  };
  const handlesetName = (name) => {
    if(name){
      setName(name._id);
      setEmail(name.email)
      setPhone(name.phone)
      handleChange("deliveryname", name.name)
      handleChange("deliveryemail", name.email)
      handleChange("deliveryphone", name.phone)
      handleLocationChange(name.address)
    }
  };
  const handleLocationChange = (location) => {
    handleChange("deliverylocation", location)
    setDeliveryLocation(location)
    setAddress(location.address)
  }
  return (
    <Card className={`shadow border-0 ${cardStyle}`}>
      <CardHeader className="bg-primary text-white">Delivery details</CardHeader>
      <CardBody>
      <div style={{padding : 2}}>
          <form className="row" noValidate autoComplete="off">
              <div className="col-md-12 col-12" >
                <Name setName={handlesetName}/>
              </div>
              <div className="col-md-12 col-12" >
                <label>Phone</label>
              <PhoneInput
                  label="Phone"
                  country={'ke'}
                  value={phone}
                  onChange={(phone) => handlePhoneChange(phone)}
                />
              </div>
              <div className="col-md-6 col-12" >
                <TextField
                  id="email"
                  label="Email"
                  value={email}
                  onChange={handleEmailChange}
                  margin="normal"
                  fullWidth
                  variant="outlined"
                />
              </div>
              <div className="col-md-6 col-12" >
                <TextField
                  id="orderid"
                  label="Order ID"
                  value={order}
                  onChange={handleOrderChange}
                  margin="normal"
                  fullWidth
                  variant="outlined"
                />
              </div>
              {/* <div className="col-md-12 col-12" >
                <SearchPlace setLocation={handleLocationChange} text={"Delivery Location"}/>
              </div> */}
              <div className="col-md-12 col-12" >
              <TextField
                  id="deliverylocation"
                  label="Delivery Location"
                  value={address}
                  margin="normal"
                  fullWidth
                  variant="outlined"
                />
              </div>
              <div className="col-md-12 col-12" >
              <div key="datetime_default" className="picker">
              <label>Deliver before</label>
              <DateTimePicker
                fullWidth
                value={selectedDate}
                showTabs={false}
                variant="outlined"
                onChange={handleDateChange}
                leftArrowIcon={<i className="zmdi zmdi-arrow-back"/>}
                rightArrowIcon={<i className="zmdi zmdi-arrow-forward"/>}
              />
              </div>
              </div>
              <div className="col-md-12 col-12" >
                <TextField
                  id="Description"
                  label="desc"
                  value={desc}
                  onChange={handleDescChange}
                  margin="normal"
                  fullWidth
                  variant="outlined"
                />
              </div>
          </form>
        </div> 
      </CardBody>
      <CardFooter>Delivery details</CardFooter>
    </Card>
  );
};

const styles = theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
});

class SimpleExpansionPanel extends Component {
  constructor(props){
    super(props)
    this.state = {
      pickupname : '',
      pickphone : '',
      pickorderid : '',
      pickemail : '',
      picklocation : {},
      pickbefore : new Date(),
      pickdesc : '',
      deliveryname : '',
      deliveryphone : '',
      deliveryorderid : '',
      deliveryemail : '',
      deliverylocation : {},
      deliverbefore : new Date(),
      deliverydesc : '',
      agent_id: "",
      loading: false,
      success: false,
      error: false,
    }
  }
  handleChange = (name , value) => {
    console.log(value)
    this.setState({
      [name]: value,
    });
  };
  handleButtonClick = () => {
    this.setState({loading : true})
    const data={
      agent_id: this.state.agent_id,
      pickup : [
        {
          pickupname : this.state.pickupname,
          pickphone : this.state.pickphone,
          pickorderid : this.state.pickorderid,
          pickemail : this.state.pickemail,
          picklocation : this.state.picklocation,
          pickbefore : this.state.pickbefore,
          pickdesc : this.state.pickdesc,
        }
      ],
      delivery : [
        {
          deliveryname : this.state.deliveryname,
          deliveryphone : this.state.deliveryphone,
          deliveryorderid : this.state.deliveryorderid,
          deliveryemail : this.state.deliveryemail,
          deliverylocation : this.state.deliverylocation,
          deliverbefore : this.state.deliverbefore,
          deliverydesc : this.state.deliverydesc,
        }
      ]
    }
    console.log(JSON.stringify(data))
    this.props.fetchStart()
    axios.post('tasks',data, { handlerEnabled: true }
    ).then(({data}) => {
      console.log("postTask: ", data);
      if (data.status==201) {
        this.setState({loading : false, success : true})
        this.props.fetchSuccess()
        const payload = {date : this.props.selectedDate}
        this.props.getTasks(payload)
        this.props.submitted()
      } else {
        this.setState({loading : false, success : false, error: true})
        this.props.fetchError("Error occured")
      }
    }).catch((error) => {
      this.setState({loading : false, success : false, error: true})
        console.log(JSON.stringify(error))
        this.props.fetchError("Error occured")
        console.log("Error****:", error.message);
    });
  };
  render () {
    const {classes} = this.props;
    return (
      <div>
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
          <Typography className={classes.heading}>Pick Up</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <PickupCard handleChange={this.handleChange} setPickLocation={this.props.setPickLocation}/>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
          <Typography component="h2" variant="display1" className={classes.heading}>Delivery</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <DeliveryCard handleChange={this.handleChange} setDeliveryLocation={this.props.setDeliveryLocation}/>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
          <Typography component="h2" variant="display1" className={classes.heading}>Assign Agent</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <CheckboxListSecondary handleChange={this.handleChange}/>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <CircularFab handleButtonClick={this.handleButtonClick} loading={this.state.loading} success={this.state.success} error={this.state.error}/>
      <InfoView/>
      </div>
  );
}
}

SimpleExpansionPanel.propTypes = {
  classes: PropTypes.object.isRequired,
};
const mapDispatchToProps = dispatch => {
  return {
    fetchError: (payload) => dispatch(fetchError(payload)),
    fetchStart: () => dispatch(fetchStart()),
    fetchSuccess: (id, title) => dispatch(fetchSuccess()),
    getTasks: (payload) => dispatch(getTasks(payload))
  }
}
const mapStateToProps = (state, ownProps) => {
  
  return {}
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(SimpleExpansionPanel));