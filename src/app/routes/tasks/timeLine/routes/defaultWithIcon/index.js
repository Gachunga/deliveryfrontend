import React from 'react';
import timeLineData from '../timeLineData';
import {Accessible, AccountCircle, AddShoppingCart, EventSeat, Tablet} from '@material-ui/icons';
import ContainerHeader from 'components/ContainerHeader/index';
import IntlMessages from 'util/IntlMessages';
import Divider from '@material-ui/core/Divider';
import moment from 'moment';
import {Badge} from 'reactstrap';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import VisibilityIcon from '@material-ui/icons/Visibility';

const WithIconTimeLineItem = ({styleName, color, pick, children}) => {
  return (
        <div className={`timeline-item timeline-time-item ${styleName}`}>
          <div className="timeline-time" style={{alignSelf : "flex-start"}}>{moment(pick.pickbefore).format("Do, h:mm:ss a")}</div>
          <div className={`timeline-badge bg-${color}`}>{children}</div>
          <div className="timeline-panel">
            <h4 className={`timeline-tile text-${color}`}>{pick.picklocation.address}</h4>
            <p>{pick.pickdesc}</p>
            {pick.status=="assigned"&&<Badge color="warning" pill>{pick.status}</Badge>}
            {pick.status=="accepted"&&<Badge color="primary" pill>{pick.status}</Badge>}
            {pick.status=="started"&&<Badge color="secondary" pill>{pick.status}</Badge>}
            {pick.status=="success"&&<Badge color="success" pill>{pick.status}</Badge>}
          </div>
        </div>
  )
};
const WithIconDeliveryTimeLineItem = ({styleName, color, deliver, children}) => {
  return(
    <div className={`timeline-item timeline-time-item`}>
      <div className="timeline-time">{moment(deliver.deliverbefore).format("Do, h:mm:ss a")}</div>
      <div className={`timeline-badge bg-${color}`}>{children}</div>
      <div className="timeline-panel">
        <h4 className={`timeline-tile text-${color}`}>{deliver.deliverylocation.address}</h4>
        <p>{deliver.deliverydesc}</p>
        {deliver.status=="assigned"&&<Badge color="warning" pill>{deliver.status}</Badge>}
            {deliver.status=="accepted"&&<Badge color="primary" pill>{deliver.status}</Badge>}
            {deliver.status=="started"&&<Badge color="secondary" pill>{deliver.status}</Badge>}
            {deliver.status=="success"&&<Badge color="success" pill>{deliver.status}</Badge>}
      </div>
    </div>
  )
  };
const DefaultWithIcon = (props) => {
  const data =props.data
  return (
    data.map((value, index) => {
      const {pickup, delivery} = value;
      return(<div>
        <div className="timeline-section timeline-center clearfix animated slideInUpTiny animation-duration-3">
        <Button
        variant="contained"
        color="warning"
        startIcon={<VisibilityIcon />}
        onClick={() => {props.setMapLineFromTasks(value)}}
        >
          Map
        </Button>
      
        {pickup.map((pick, index) => {
          return(<WithIconTimeLineItem pick={pick} color="pink">
            <p>P</p>
          </WithIconTimeLineItem>)
          })}
           <Button
            onClick={() => {this.props.openLeftPanel(false)}}
            >
          <h4 className={`timeline-tile text-purple`}>{value.agent.name}</h4>
          </Button>
          {delivery.map((deliver, index) => {
          return(<WithIconDeliveryTimeLineItem styleName="timeline-inverted" deliver={deliver} color="purple">
            <p>D</p>
          </WithIconDeliveryTimeLineItem>)
          })}
        </div>
        
        <Divider/>
      </div>)
    })
    
  )
};

export default DefaultWithIcon;

