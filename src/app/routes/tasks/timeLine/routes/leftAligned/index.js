import React from 'react';
import timeLineData from '../timeLineData';
import ZigzagTimeLineItem from 'components/timeline/ZigzagTimeLineItem';
import ContainerHeader from 'components/ContainerHeader/index';
import IntlMessages from 'util/IntlMessages';

const LeftAligned = () => {
  return (
    <div>
      <div className="timeline-section clearfix animated slideInUpTiny animation-duration-3">
        {timeLineData.map((timeLine, index) => <ZigzagTimeLineItem key={index} timeLine={timeLine}/>)}
      </div>
    </div>
  )
};

export default LeftAligned;

