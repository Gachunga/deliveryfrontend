import firebase from 'firebase'

// Initialize Firebase
const config = {
  apiKey: "AIzaSyB_k_l9nbwoowL0ZV6BeZtlAoRRZsSiiKQ",
  authDomain: "delivery-74086.firebaseapp.com",
  databaseURL: "https://delivery-74086.firebaseio.com",
  projectId: "delivery-74086",
  storageBucket: "delivery-74086.appspot.com",
  messagingSenderId: "759637315902",
  appId: "1:759637315902:web:d703b3b77db81ef4cd1c6f",
  measurementId: "G-HD675PJDDF"
};

firebase.initializeApp(config);
const auth = firebase.auth();

const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
const facebookAuthProvider = new firebase.auth.FacebookAuthProvider();
const githubAuthProvider = new firebase.auth.GithubAuthProvider();
const twitterAuthProvider = new firebase.auth.TwitterAuthProvider();

const database = firebase.database();
export {
  auth,
  database,
  googleAuthProvider,
  githubAuthProvider,
  facebookAuthProvider,
  twitterAuthProvider
};