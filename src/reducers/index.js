import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router'
import Settings from './Settings';
import Auth from './Auth';
import Common from './Common';
import SaleStructure from './SaleStructure';
import categories from './categories';
import customers from './customers';
import salesreps from './salesreps';
import tasks from './tasks';

export default (history) => combineReducers({
  router: connectRouter(history),
  settings: Settings,
  auth: Auth,
  commonData: Common,
  salestructure: SaleStructure,
  categories : categories,
  customers : customers,
  salesreps : salesreps,
  tasks :tasks
});
