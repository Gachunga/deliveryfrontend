import {
    HIDE_MESSAGE,
    INIT_URL,
    ON_HIDE_LOADER,
    ON_SHOW_LOADER,
    TASKS_DATA,
    AGENTS_DATA
} from "constants/ActionTypes";

const INIT_STATE = {
    loader: false,
    alertMessage: '',
    showMessage: false,
    initURL: '',
    refesh : false,
    tasksdata: [],
    agentsdata: [],
};


export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case TASKS_DATA: {
            return {
                ...state,
                loading: false,
                tasksdata: action.payload,
            }
        }
        case AGENTS_DATA: {
            return {
                ...state,
                loading: false,
                agentsdata: action.payload,
            }
        }
        
        default:
            return state;
    }
}
