import axios from 'axios';
import { BASE_URL } from '../constants/ActionTypes'
import configureStore, {history} from '../store';
import { push } from 'connected-react-router'

export const store = configureStore();

var headers = {};

  headers = {
    'Content-Type': 'application/json',
    'Accept' : 'application/json',
  }


const axiosInstance = axios.create({
  baseURL: BASE_URL,//YOUR_API_URL HERE
  headers: headers
});
const isHandlerEnabled = (config={}) => {
  return config.hasOwnProperty('handlerEnabled') && !config.handlerEnabled ? 
    false : true
}
const requestHandler = (request) => {
  const token = localStorage.token
  console.log(token)
  if (isHandlerEnabled(request)) {
    // Modify request here
    if(token){
      let newToken = token
      if(isJson(token)){
        newToken = JSON.parse(token)
      }
      request.headers['Authorization'] = 'Bearer '+ newToken 
      console.log(request.headers)
    }
  }
  return request
}
function isJson(str) {
  try {
      JSON.parse(str);
  } catch (e) {
      return false;
  }
  return true;
}
axiosInstance.interceptors.request.use(
  request => requestHandler(request)
)
const errorHandler = (error) => {
  console.log(error)
  // console.log(error.response.data)
  if (isHandlerEnabled(error.config)) {
    // Handle errors
    if(error.response){
    if (error.response.data.status === 401) {
      localStorage.removeItem("token")
      localStorage.removeItem("user")
      store.dispatch({type:"reset_auth"})
      store.dispatch(push('/signin'))
      return new Promise((resolve, reject) => {
        reject(error);
      });
    }
  }
  }
  return Promise.reject({ ...error })
}

const successHandler = (response) => {
  if (isHandlerEnabled(response.config)) {
    // Handle responses
  }
  return response
}
axiosInstance.interceptors.response.use(
  response => successHandler(response),
  error => errorHandler(error)
)
export default axiosInstance

